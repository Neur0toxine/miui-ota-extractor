try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
    import urllib.parse
import os, base64, json, threading, sys, urllib
from Cryptodome.Cipher import AES

if(os.sys.platform == 'win32'):
	os.system("title MIUI OTA Extractor")

print("MIUI OTA Extractor is running.")
print("Be VERY attentive. One incorrect symbol may ruin all!\n")

global checkurl, mode, miui_key, miui_iv, block
checkurl = 'http://update.miui.com/updates/miotaV3.php'
mode = AES.MODE_CBC
miui_key = 'miuiotavalided11'.encode()
miui_iv = '0102030405060708'.encode()
block = 16

region = 'MI'
build = 'F'
andr_ver = "7.1.2"
device_code = "rosy"
locale = "en_US"
chipset = "sdm450"
bl = "1"
fw_ver = "MIUI-V9.5.7.0.NDAMIFA"

def einp(msg, df):
	v = input(msg)
	if(v != ''):
		return v
	else:
		return df

print("> First, define your region. For example: devices for global sale marked as MI, for China only - CN, for Russia - RU")
region = einp("* Define region of your device (default - " + region + "): ", region)
print("> Your entered: " + region + "\n")

print('> Now you must define firmware type: Print "F" for stable builds, or "X" for daily builds.')
build = einp("* Define ROM type (default - " + build + "): ", build)
print('> You entered: ' + build + "\n")

print('> So, provide your Android version.')
andr_ver = einp("* Android version (default - " + andr_ver + "): ", andr_ver)
print('> You entered: ' + andr_ver + "\n")

print('> Next you must provide codename of your device. For example: vince - Redmi 5 Plus, rosy - Redmi 5')
device_code = einp("* Provide device codename (default - " + device_code + "): ", device_code if(region == 'CN') else device_code + '_global')
print('> You entered: ' + device_code.replace('_global', '') + "\n")

print('> We need your locale for this step.')
locale = einp("* Set your locale (default - " + locale + "): ", locale)
print('> You entered: ' + locale + "\n")

print("> It can seems hard, but it's neccessary part. Provide your device chipset. For example: Redmi 5 (rosy) have sdm450")
chipset = einp("* Set your chipset (default - " + chipset + "): ", chipset)
print('> You entered: ' + chipset + "\n")

print("> What's about bootloader?")
bl = "1" if input('* Did you unlocked it [Y/N]? ').lower().strip() == "y" else "0"
print("> You have " + ("un" if(bl == "1") else "") + "locked bootloader.\n")

print("> And most important part. Provide your current MIUI version. Note: if you want to extract closed beta" +
" - provide it's version, not your current firmware, otherwise you'll receive link to current stable version.")
print("> For example: current firmware version - 'MIUI-V9.2.7.0.NDAMIEK'. You want to receive closed beta version, " +
"marked as 'MIUI-V9.5.7.0.NDAMIFA'. Provide 'MIUI-V9.5.7.0.NDAMIFA' version, not 'MIUI-V9.2.7.0.NDAMIEK', or you " + 
"will give current stable version, not closed beta.")
fw_ver = einp("* MIUI version (default - " + fw_ver + "): ", fw_ver)
print('> You entered: ' + fw_ver + "\n")

print('All things is OK, now trying to find update...\n')


default = {
    "a": "0", # Don't know what this is.
    "c": andr_ver, # Same as 'c' above, it's the Android version.
    "b": build, # Same as above, 'X' for weekly build.
    "d": device_code, # The device name, same as above, chiron for Chinese, chiron_global for global.
    "g": "00000000000000000000000000000000", # This seems to be the android_id of the device. Maybe encoded somehow.
    "cts": "0", # I don't know what this is.
    "i": "0000000000000000000000000000000000000000000000000000000000000000", # This seems to be the imei of the device, obviously encoded somehow.
    "isR": "0", # I don't know what this is.
    "f": "1", # I don't know what this is.
    "l": locale, # The locale.
    "n": "",  # I don't know what this parameter is
    "sys": "0", # I don't know what this is.
    "p": chipset, # The chipset
    "unlock": bl,  # 1 means bootloader is unlocked. 0 means locked.
    "r": region, # I don't know what this is, maybe region of device?
    "sn": "0x00000000", # Probably the serial number of the device, maybe encoded somehow.
    "v": fw_ver, # The version of MIUI installed.
    "bv": "9", # I don't know what this is.
    "id": "", # I don't' know what this is.
} 

def encrypt(val):
    rjn = AES.new(miui_key, mode, miui_iv)
    n = block - (len(val) % block)
    while (block - (len(val) % block) != block):
        val += chr(n)
    encrypted = base64.b64encode(rjn.encrypt(val.encode()))
    return encrypted

def decrypt(val):
    rjn = AES.new(miui_key, mode, miui_iv)
    decrypted = rjn.decrypt(base64.b64decode(val)).decode('utf-8')
    pos = decrypted.rfind('}')
    if(pos != -1):
        return decrypted[0:pos+1]
    else:
        return decrypted

def fw_info(rom_title, rom_info, mirrors):
    if(rom_info == []):
        print(rom_title + ' ROM info is missing.')
    else:
        print('\n=== ' + rom_title + ' ROM ===')
        print("Name: " + rom_info['name'])
        print('Changelog: ' + rom_info['descriptionUrl'])
        print('Codebase: '+ rom_info['codebase'])
        print('Version: ' + rom_info['version'])
        print('Filename: "' + rom_info['filename'] + '"')
        print('Size: ' + rom_info['filesize'])
        print('MD5 Hash: ' + rom_info['md5'])
        for x in mirrors:
            print('Link: ' + x + '/' + rom_info['version'] + '/' + rom_info['filename'])

def makeRequest(inp, tval="", sval="1"):
    js = json.dumps(inp, separators=(',', ':'))
    global svalue
    payload = ''
    try:
        payload = urllib.quote_plus(encrypt(js))
    except:
        payload = urllib.parse.quote(encrypt(js))
    postdata = "q="+payload+"&t="+tval+"&s=" + sval
    global checkurl
    response = urllib2.urlopen(checkurl, postdata.encode('utf-8'))
    ret = response.read()
    r = decrypt(ret)
    goods = ''
    try:
        goods = json.loads(r)
        mirrors = goods['MirrorList']
        fw_info('Current', goods['CurrentRom'], mirrors)
        fw_info('Latest', goods['LatestRom'], mirrors)
        fw_info('Incremental', goods['IncrementRom'], mirrors)
    except:
        print('\n> Looks like something goes wrong, or you filled quiz with incorrect information :(\nTry again!')
        
makeRequest(default)

# MIUI OTA Extractor
This script can extract MIUI ROM's from Xiaomi OTA servers.   
**Install**:
```
pip3 install -r requirements.txt
python3.5 app.py
```
Thanks to XDA Community for making this possible.